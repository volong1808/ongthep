<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ongthep' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '123456' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'G]h?!DIX.NhyRY189sdYN~Vq+M.0#H%()C4sG z{?DYeI:and5OtO|yVaXYQ/8}M' );
define( 'SECURE_AUTH_KEY',  '*mVwb_Ab^r$Mlu,9Ca.`#Mv7z>`3WuFSi8|-}gwYX*HRo.[w+73D:?P}+~UQu_1=' );
define( 'LOGGED_IN_KEY',    ')c=0m/7k!1xvt0=Mj=ct)$tp&ohhruCk,ysXfLu`l18eX3;:VmRbv(T`{K41KH i' );
define( 'NONCE_KEY',        '$:KC3T(:mskoKP&7$;t*D3[i.tq.m]Gp{VVULUVBKAyioal!0XK(:{4J@6Nu+O@s' );
define( 'AUTH_SALT',        '.=U; N%@} G`AJu*w6!=3mw6e1f%-F-Rz<lN!IP/~^eZ&B[2gJ;/I#vcom,~r5`B' );
define( 'SECURE_AUTH_SALT', '!fw*x}q)D6xjKH1>Ft;eauQtmyE]lYR:^&hz?q%#}-BTnT%W7[,DKQ`lL#%N? lg' );
define( 'LOGGED_IN_SALT',   'rqM8=Vs!_TJ=sL*YuYiiZU>i|;o;]FZ>u0gu^kWP4)dhE^XF^<<#$:=bdxOXtrwq' );
define( 'NONCE_SALT',       'R<B49uYZ| z*;u4#zyOkO-it!qIshiO(BkCPbd4#?Jc/Ul^7NN=3sln/Ez}Y1;<#' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
