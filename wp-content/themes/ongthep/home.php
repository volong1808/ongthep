<?php
get_header();
$get_template_directory = get_template_directory_uri();
$current_lang = get_bloginfo('language');
?>
<?php
$slide_args = array(
    'post_type' => 'slide',
    'orderby' => 'updated_at',
    'order' => 'DESC'
);
$slide_query = new WP_Query($slide_args);
?>
<!-- Slide -->
<div>
    <!-- Swiper -->
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php if ($slide_query->have_posts()) :
                while ($slide_query->have_posts()): ?>
                    <?php
                    $slide_query->the_post();
                    $slide_img = get_the_post_thumbnail_url($post);
                    $description = get_field('description');
                    $link = get_field('link');
                    ?>
                    <div class="swiper-slide">
                        <div class="baogia">
                            <a href="<?php echo home_url('/lien-he')?>">Yêu cầu Báo Giá <i class="icofont-swoosh-right"></i></a>
                        </div>
                        <img class="w-100" src="<?php echo $slide_img ?>"/>
                    </div>
                <?php endwhile; endif; ?>
            <?php wp_reset_query() ?>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>
<div id="content">
    <!-- module file -->
    <!--    <div class="content_file">-->
    <!--        <div class="item_file file1 col-xl-4 col-lg-4  col-md-4 col-sm-12 ">-->
    <!--            <h3>Tải xuống danh mục sản phẩm</h3>-->
    <!--        </div>-->
    <!--        <div class="item_file file2 col-xl-4 col-lg-4  col-md-4 col-sm-12 ">-->
    <!--            <h3>Tải xuống catologe</h3>-->
    <!--        </div>-->
    <!--        <div class="item_file file3 col-xl-4 col-lg-4  col-md-4 col-sm-12 ">-->
    <!--            <h3>Xem Video Sản Xuất</h3>-->
    <!--        </div>-->
    <!--        <div class="clear"></div>-->
    <!--    </div>-->
    <!-- /module file -->

    <!-- module product -->
    <div class="product">
        <div class="container">
            <div class="intro_product">
                <div class="intro_left col-xl-5 col-lg-5  col-md-12 col-xs-12 ">
                    <p>Chúng tôi luôn sẵn sàng phục vụ các dịch vụ tốt nhất cho các sản phẩm đường ống công nghiệp.</p>
                </div>
                <div class="intro_right col-xl-7 col-lg-7  col-md-12 col-xs-12 ">
                    <p>Chất lượng sản phẩm của chúng tôi, được thiết kế bởi chúng tôi đã được ca ngợi và chứng nhận bởi
                        nhiều công ty và MNC trên toàn thế giới. Kể từ khi giao dịch với những khách hàng thành công
                        nhất, chúng tôi đã nhận ra yêu cầu toàn cầu & nhu cầu về chất lượng vượt trội của nó.</p>
                </div>
                <div class="clear"></div>
            </div>
            <div class="box_product">
                <?php
                $home_product_args = array(
                    'post_type' => 'san-pham',
                    'orderby' => 'updated_at',
                    'posts_per_page' => 9,
                    'order' => 'DESC'
                );
                $home_product_query = new WP_Query($home_product_args);
                ?>
                <div class="container">
                    <div class="row display-flex">
                        <?php if ($home_product_query->have_posts()): ?>
                            <?php while ($home_product_query->have_posts()): ?>
                                <?php
                                $home_product_query->the_post();
                                $home_product_url = get_permalink($post->ID);
                                $home_product_img = get_the_post_thumbnail_url($post, 'product_thumb');
                                $home_product_title = $post->post_title;
                                $home_product_description = get_field('description');
                                ?>

                                <div class="item_product col-xl-4 col-lg-4 col-md-4 col-xs-12">
                                    <img src="<?php echo $home_product_img ?>" alt="<?php echo $home_product_title ?>">
                                    <div class="info_item">
                                        <h3 class="name_item"><a
                                                    href="<?php echo $home_product_url ?>"><?php echo $home_product_title ?></a>
                                        </h3>
                                        <div class="mota_item">
                                            <?php echo nl2br(strip_tags($home_product_description)) ?>
                                        </div>
                                        <div class="chitiet_item"><a href="<?php echo $home_product_url ?>">Chi Tiết <i
                                                        class="icofont-swoosh-right"></i></a></div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <!-- /module product -->

    <!-- module service -->

    <div class="dichvu">
        <div class="container">
            <?php
            $page_services_args = array(
                'post_type' => 'page',
                'name' => 'dich-vu',
                'post_status' => 'publish'
            );
            $page_services_query = new WP_Query($page_services_args);
            ?>
            <?php if ($page_services_query->have_posts()): ?>
                <?php while ($page_services_query->have_posts()): ?>
                    <?php
                    $page_services_query->the_post();
                    $page_service_url = get_permalink($post->ID);
                    $page_service_title = $post->post_title;
                    $page_service_description = get_field('description');
                    ?>
                    <div class="title">
                        <h3><a href="<?php echo $page_service_url ?>"><?php echo $page_service_title ?></a></h3>
                        <p class="intro_title"><?php echo $page_service_description ?></p>
                    </div>
                <?php endwhile; ?>
            <?php else: ?>
                <div class="title">
                    <h3>Dịch Vụ</h3>
                    <p class="intro_title">Chúng tôi là chuyên gia trong việc nhập khẩu và kinh doanh các loại sắt thép
                        trong xây dựng công nghiệp ,xây dựng dân dụng,cơ khí chế tạo và đóng tầu, ...v.v các loại mặt
                        hàng như: Thép tấm đặc chủng, thép tấm chế tạo khuôn mẫu, thép tấm inox, đồng tấm, đồng ống, ống
                        thép , ống đúc, ống hàn, ống mạ kẽm, rây tầu, sắt hộp, sắt hình ,H, U, V,….v.v và nhận gia công
                        trong lĩnh vực cơ khí</p>
                </div>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <div class="box_dichvu">
                <?php
                $home_services_args = array(
                    'post_type' => 'dich-vu',
                    'orderby' => 'updated_at',
                    'posts_per_page' => 4,
                    'order' => 'DESC'
                );
                $home_services_query = new WP_Query($home_services_args);
                ?>
                <?php if ($home_services_query->have_posts()): ?>
                    <?php while ($home_services_query->have_posts()): ?>
                        <?php
                        $home_services_query->the_post();
                        $home_service_url = get_permalink($post->ID);
                        $home_service_img = get_the_post_thumbnail_url($post, 'product_thumb');
                        $home_service_title = $post->post_title;
                        ?>
                        <div class="item_dv col-xl-3 col-lg-3  col-md-6 col-xs-12">
                            <img src="<?php echo $home_service_img ?>" alt="<?php echo $home_service_title ?>">
                            <h3 class="name_dv"><a
                                        href="<?php echo $home_service_url ?>"><?php echo $home_service_title ?></a>
                            </h3>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
<!-- /module service -->

<!-- module news -->
<div class="tintuc">
    <div class="container">
        <div class="tintuc_left col-md-12 -col-xs-12 col-sm-12">
            <?php
            $page_news_args = array(
                'post_type' => 'page',
                'name' => 'tin-tuc',
                'post_status' => 'publish'
            );
            $page_news_query = new WP_Query($page_news_args);
            ?>
            <?php if ($page_news_query->have_posts()): ?>
                <?php while ($page_news_query->have_posts()): ?>
                    <?php
                    $page_news_query->the_post();
                    $page_news_url = get_permalink($post->ID);
                    $page_news_title = $post->post_title;
                    ?>
                    <div class="title">
                        <h3><a href="<?php echo $page_news_url ?>"><?php echo $page_news_title ?></a></h3>
                    </div>
                <?php endwhile; ?>
            <?php else: ?>
                <div class="title">
                    <h3>Tin Tức</h3>
                </div>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <div class="box_tintuc">
                <div class="slider_tin">
                    <?php
                    $home_news_args = array(
                        'post_type' => 'tin-tuc',
                        'orderby' => 'updated_at',
                        'posts_per_page' => 6,
                        'order' => 'DESC'
                    );
                    $home_news_query = new WP_Query($home_news_args);
                    ?>
                    <?php if ($home_news_query->have_posts()): ?>
                        <?php while ($home_news_query->have_posts()): ?>
                            <?php
                            $home_news_query->the_post();
                            $home_news_url = get_permalink($post->ID);
                            $home_news_img = get_the_post_thumbnail_url($post, 'product_thumb');
                            $home_news_title = $post->post_title;
                            $home_news_date = $post->post_date;
                            ?>

                            <div class="man_news_grid_item">
                                <a href="<?php echo $home_news_url; ?>" class="man_news_item_link"></a>
                                <div class="man_news_item_img">
                                    <img src="<?php echo $home_news_img; ?>" alt="">
                                    <div class="man_news_item_over"></div>
                                    <div class="man_news_item_cont">
                                        <div class="man_news_item_title">
                                            <h3><?php echo $home_news_title; ?></h3>
                                        </div>
                                        <div class="man_news_item_date"><i class="icofont-clock-time"></i>
                                            <span><?php echo $home_news_date; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /module news -->

<!-- module about -->
<div class="gioithieu">
    <div class="container">
        <div class="gioithieu_left col-xl-6 col-lg-6  col-md-6 col-xs-12">
            <div class="title">
                <h3>Về Chúng Tôi</h3>
            </div>
            <div class="box_gioithieu">
                <p><?php echo get_option('description') ?></p>
                <?php
                $about_args = array(
                    'post_type' => 'page',
                    'posts_per_page' => 1,
                    'name' => 'gioi-thieu'

                );
                $about_query = new WP_Query($about_args);
                if ($about_query->have_posts()):
                    ?>
                    <?php while ($about_query->have_posts()): ?>
                    <?php
                    $about_query->the_post();
                    $page_url = get_permalink($post->ID);
                    ?>
                    <div class="xemchitiet"><a href="<?php echo $page_url ?>">Chi Tiết <i
                                    class="icofont-swoosh-right"></i></a></div>

                <?php endwhile; ?>
                <?php endif ?>
                <?php wp_reset_query(); ?>

            </div>
        </div>
        <div class="gioithieu_right col-xl-6 col-lg-6  col-md-6 col-xs-12">
            <div class="title">
                <h3>Đối Tác Của Chúng Tôi</h3>
            </div>
            <?php get_template_part('template-parts/content', 'partner'); ?>
        </div>
        <div class="clear"></div>
    </div>
</div>
<!-- module about -->

<!-- module quang cao -->
<!--<div class="quangcao">-->
<!--    <div class="container">-->
<!--        <span>Giảm Giá 40% Các Sản Phẩm Phổ Biến Của Chúng Tôi.</span>-->
<!--        <strong><a href="">Xem Ưu Đãi <i class="icofont-swoosh-right"></i></a></strong>-->
<!--    </div>-->
<!--</div>-->
<!-- /module quang cao -->
</div>
<?php
get_footer();
?>
