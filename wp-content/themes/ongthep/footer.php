<?php  $get_template_directory = get_template_directory_uri(); ?>

<div id="fooder">
    <div class="container">
        <div class="row">
            <div class="col-md-3 footer-top-item">
                <div class="footer-top-item__head">
                    <h3 class="footer-top-item__title">Liên Hệ</h3>
                </div>
                <div class="footer-top-item__body footer-info">
                    <h2 class="footer-info__name">
                        <?php echo get_option('company_name') ?>
                    </h2>
                    <div class="footer-info__item">
                        <i class="icofont-location-pin"></i>
                        <span class="footer-info__label">Địa Chỉ:</span>
                        <span class="footer-info__text"><?php echo get_option('address') ?></span>
                    </div>
                    <div class="footer-info__item">
                        <i class="icofont-phone"></i>
                        <span class="footer-info__label">Hotline:</span>
                        <span class="footer-info__text"><a href="tel:<?php echo get_option('hot_line1') ?>"><?php echo get_option('hot_line1') ?></a></span>
                        <?php
                        if (!empty(get_option('user_contact'))) {
                            echo "<span>&nbsp;-&nbsp;". get_option('user_contact') ."</span>";
                        }
                        ?>
                    </div>
                    <div class="footer-info__item">
                        <i class="icofont-send-mail"></i>
                        <span class="footer-info__label">E-mail:</span>
                        <span class="footer-info__text"><a href="mailto:<?php echo get_option('email') ?>"><?php echo get_option('email') ?></a></span>
                    </div>
                    <div class="footer-info__item">
                        <div class="icon_share">
                            <ul>
                                <li><a class="fb" href="<?php echo get_option('facebook_page') ?>" target="_blank"><i class="icofont-facebook"></i></a></li>
                                <li><a class="tt" href="<?php echo get_option('twitter_page') ?>" target="_blank"><i class="icofont-twitter"></i></a></li>
                                <li><a class="yt" href="<?php echo get_option('youtube_channel') ?>" target="_blank"><i class="icofont-youtube-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 footer-top-item">
                <div class="footer-top-item__head">
                    <h3 class="footer-top-item__title">Sản phẩm mới</h3>
                </div>
                <div class="footer-top-item__body footer-video">
                    <?php
                    $product_args = array(
                        'post_type' => 'san-pham',
                        'posts_per_page' => 5
                    );
                    $product_query = new WP_Query($product_args);
                    ?>
                    <?php if ($product_query->have_posts()): ?>
                        <ul class="field-footer__list">
                            <?php while ($product_query->have_posts()): ?>
                                <?php
                                $product_query->the_post();
                                $product_url = get_permalink($post->ID);
                                ?>
                                <li class="field-footer__item">
                                    <i class="icofont-arrow-right"></i>
                                    <a href="<?php echo $product_url ?>">
                                        <?php echo $post->post_title; ?>
                                    </a>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
            <div class="col-md-3 footer-top-item">
                <div class="footer-top-item__head">
                    <h3 class="footer-top-item__title">Dịch vụ</h3>
                </div>
                <div class="footer-top-item__body footer-video">
                    <?php
                    $service_args = array(
                        'post_type' => 'dich-vu',
                        'posts_per_page' => 5
                    );
                    $service_query = new WP_Query($service_args);
                    ?>
                    <?php if ($service_query->have_posts()): ?>
                        <ul class="field-footer__list">
                            <?php while ($service_query->have_posts()): ?>
                                <?php
                                $service_query->the_post();
                                $service_url = get_permalink($post->ID);
                                ?>
                                <li class="field-footer__item">
                                    <i class="icofont-arrow-right"></i>
                                    <a href="<?php echo $service_url ?>">
                                        <?php echo $post->post_title; ?>
                                    </a>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
            <div class="col-md-3 footer-top-item">
                <div class="footer-top-item__head">
                    <h3 class="footer-top-item__title">Tin Tức</h3>
                </div>
                <div class="footer-top-item__body footer-video">
                    <?php
                    $news_args = array(
                        'post_type' => 'tin-tuc',
                        'posts_per_page' => 3
                    );
                    $news_query = new WP_Query($news_args);
                    ?>
                    <?php if ($news_query->have_posts()): ?>
                        <ul class="field-footer__list">
                            <?php while ($news_query->have_posts()): ?>
                                <?php
                                $news_query->the_post();
                                $news_url = get_permalink($post->ID);
                                $news_img = get_the_post_thumbnail_url($post, 'icon');
                                ?>
                                <li class="field-footer__item news">
                                    <img width="60" src="<?php echo $news_img; ?>" alt="<?php echo $post->post_title; ?>">
                                    <div class="text">
                                        <a href="<?php echo $news_url ?>">
                                            <?php echo $post->post_title; ?>
                                        </a>
                                        <p><?php echo $post->post_date; ?></p>
                                    </div>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="copyright">
    <div class="container">
        <p class="text_copyright">
            © 2020 Design by <a href="http://sammedia.net">SamMedia</a>
        </p>
    </div>
</div>
</div><!-- #content -->
</div><!-- #page -->

<?php wp_footer(); ?>

<a id="scroll-top">
    <i class="fa fa-angle-double-up" aria-hidden="true"></i>
</a>
<!--Menu -->
<script type="text/javascript" src="<?php echo $get_template_directory ?>/js/jquery.mmenu.min.all.js"></script>

<!--Menu -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="<?php echo $get_template_directory ?>/js/vendor/swiper.min.js"></script>
<!-- Custom JS -->
<script src="<?php echo $get_template_directory ?>/js/main.js"></script>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml            : true,
            version          : 'v6.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="112830733749530"
     theme_color="#0084ff"
     logged_in_greeting="Thép Kim Tín Phát Chào Bạn."
     logged_out_greeting="Thép Kim Tín Phát Chào Bạn.">
</div>

</body>
</html>
