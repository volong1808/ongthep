$(document).ready(function () {

    $m = $('nav#menu').html();
    $('nav#menu_mobi').append($m);

    $('nav#menu_mobi .search').addClass('search_mobi').removeClass('search');
    $('.hien_menu').click(function(){
        $('nav#menu_mobi').css({height: "auto"});
    });
    $('.user .fa-user-plus').toggle(function(){
        $('.user ul').slideDown(300);
    },function(){
        $('.user ul').slideUp(300);
    });

    $('nav#menu_mobi').mmenu({
        extensions	: [ 'effect-slide-menu', 'pageshadow' ],
        searchfield	: true,
        counters	: true,
        navbar 		: {
            title		: 'Menu'
        },
        navbars		: [
            {
                position	: 'top',
                content		: [ 'searchfield' ]
            }, {
                position	: 'top',
                content		: [
                    'prev',
                    'title',
                    'close'
                ]
            }
        ]
    });

    var swiper = new Swiper('.swiper-container', {
        spaceBetween: 0,
        slidesPerView: 'auto',
        centeredSlides: true,
        autoplay: {
            delay: 10000,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    $('.slider_tin').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        draggable: true,
        centerMode: false,
        arrows: true,

        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    autoplaySpeed: 1000,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 701,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    autoplaySpeed: 1000,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 520,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]

    });

    $('.feedback').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        draggable: true,
        centerMode: false,
        arrows: true,

    });

    /*Scroll Top */
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('#scroll-top').fadeIn();
        } else {
            $('#scroll-top').fadeOut();
        }
    });

    $('#scroll-top').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 300);
        return false;
    });
});