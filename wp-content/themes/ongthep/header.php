<?php $get_template_directory = get_template_directory_uri(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8"/>
    <?php wp_head(); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $get_template_directory ?>/icofont/icofont.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $get_template_directory ?>/css/style.css"/>
    <link type="text/css" rel="stylesheet" href="<?php echo $get_template_directory ?>/css/jquery.mmenu.all.css"/>
    <script type="text/javascript" src="<?php echo $get_template_directory ?>/js/jquery-3.1.0.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="<?php echo $get_template_directory ?>/css/swiper.min.css">

    <script type="text/javascript" src="<?php echo $get_template_directory ?>/slick/slick/slick.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $get_template_directory ?>/slick/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $get_template_directory ?>/slick/slick/slick-theme.css"/>

</head>
<body>
<div class="wapper">
    <!-- HEADER -->
    <div id="header">
        <div class="menu_top">
            <nav id="menu_mobi" style="height:0; overflow:hidden;"></nav>
            <div class="header">
                <div class="mobi_logo">
                    <a href="<?php echo home_url(); ?>"><img
                                src="<?php echo $get_template_directory ?>/images/logo.jpg"></a>
                </div>
                <a href="#menu_mobi" class="hien_menu">
                    <i class="icofont-listine-dots"></i>
                </a>
            </div>

            <div class="menu_pc container">
                <div class="icon_share">
                    <ul>
                        <li><a href="<?php echo get_option('facebook_page') ?>" target="_blank"><i
                                        class="icofont-facebook"></i></a></li>
                        <li><a href="<?php echo get_option('twitter_page') ?>" target="_blank"><i
                                        class="icofont-twitter"></i></a></li>
                        <li><a href="<?php echo get_option('youtube_channel') ?>" target="_blank"><i
                                        class="icofont-youtube-play"></i></a></li>
                    </ul>
                </div>
                <nav id="menu">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'menu-1'
                    ));
                    ?>
                    <div class="clear"></div>
                </nav>
            </div>

        </div>


        <div class="box_header">
            <div class="container">
                <div class="row">
                    <div class="intro_header col-xl-2 col-lg-2 col-md-2 col-sm-2 col-12 text-center">
                        <div class="header-logo">
                            <a href="<?php echo home_url(); ?>"><img
                                        src="<?php echo $get_template_directory ?>/images/logo.jpg"></a>
                        </div>
                    </div>
                    <div class="intro_header col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-left">
                        <div class="header-info">
                            <i class="icofont-send-mail"></i>
                            <div class="box-text">
                                <span class="text-info"><a
                                            href="mailto:<?php echo get_option('email') ?>"><?php echo get_option('email') ?></a></span>
                            </div>
                        </div>
                    </div>
                    <div class="intro_header col-xl-2 col-lg-2 col-md-2 col-sm-2 col-12 text-left">
                        <div class="header-info phone">
                            <i class="icofont-smart-phone"></i>
                            <div class="box-text">
                                <span class="text-info"><a
                                            href="tel:<?php echo get_option('phone') ?>"><?php echo get_option('phone') ?></a></span>
                            </div>
                        </div>
                    </div>
                    <div class="search_header col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12" style="height: 100px; display: flex">
                        <form role="search" method="get" class="search-form header-info" style="width: 100%;"
                              action="<?php echo home_url('/'); ?>">
                            <input type="search" class="search-field form-control"
                                   placeholder="<?php echo esc_attr_x('Tìm Kiếm …', 'placeholder') ?>"
                                   value="<?php echo get_search_query() ?>" name="s"
                                   title="<?php echo esc_attr_x('Search for:', 'label') ?>"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- /HEADER -->
    <!-- Content -->
    <div class="content-page">