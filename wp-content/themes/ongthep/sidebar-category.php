<section class="widget">
    <h3 class="widget-title"><span>SẢN PHẨM</span></h3>
    <div class="widget-container">
        <ul class="category">
            <?php
            $categories = get_categories();
            if (!empty($categories)): ?>
                <?php
                foreach ($categories as $category) {
                    if ($category->cat_ID != 1) : ?>
                        <li class="cate-item">
                            <a class="text-uppercase"
                               href="<?php echo get_category_link($category->cat_ID); ?>"><?php echo $category->name ?></a>
                        </li>
                    <?php endif;
                } ?>
            <?php endif; ?>
        </ul>
    </div>
</section>
