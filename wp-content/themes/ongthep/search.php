<?php get_header(); ?>
    <div class="product" style="padding-top: 0">
        <div class="post-detail content" style="padding-top: 0">
            <div class="container">
                <h3 style="border-bottom: 2px solid #000; color: #000; padding: 10px 0  ;">Kết quả tìm kiếm cho : <?php echo "$s"; ?> </h3>
                <div class="row display-flex">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php
                        $product_description = get_field('description');
                        ?>
                        <div class="item_product col-xl-4 col-lg-4 col-md-12 col-xs-12">
                            <?php the_post_thumbnail('product_thumb'); ?>
                            <div class="info_item">
                                <h3 class="name_item"><a
                                            href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h3>
                                <div class="mota_item">
                                    <?php echo nl2br(strip_tags($product_description)) ?>
                                </div>
                                <div class="chitiet_item"><a href="<?php the_permalink(); ?>">Chi Tiết <i
                                                class="icofont-swoosh-right"></i></a></div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <div class="pagination text-center">
                        <?php post_pagination(2); ?>
                    </div>
                    <?php else: ?>
                    <h4 class="text-center" style="display: block; width: 100%;">Không có sản phẩm nào!</h4>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>