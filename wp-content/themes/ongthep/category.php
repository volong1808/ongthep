<?php get_header() ?>
<?php
$get_template_directory = get_template_directory_uri();
?>
<header class="content-page__header">
</header>
<?php
$bannerImage = $get_template_directory . '/images/slider1.jpg';
?>
<div class="banner-page"
     style="background-image: url('<?php echo $bannerImage ?>')">
    <div class="banner-page__content">
        <div class="banner-page__body">
            <h2 class="banner-page__title"><?php single_cat_title(); ?></h2>
        </div>
    </div>
</div>
</header>
<?php
$current_category = get_queried_object(); ////getting current category
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$product_args = array(
    'paged' => $paged,
    'posts_per_page' => 9,
    'post_type' => 'san-pham',// your post type,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'cat' => $current_category->cat_ID // current category ID
);
$product_query = new WP_Query($product_args);
// Pagination fix
$temp_query = $wp_query;
$wp_query   = NULL;
$wp_query   = $product_query;
?>
<!-- field home -->
<div class="product">
    <div class="post-detail content">
        <div class="container">
            <?php if ($product_query->have_posts()): ?>
                <div class="row display-flex">
                    <?php while ($product_query->have_posts()): ?>
                        <?php
                        $product_query->the_post();
                        $product_url = get_permalink($post->ID);
                        $product_img = get_the_post_thumbnail_url($post, 'product_thumb');
                        $product_title = $post->post_title;
                        $product_description = get_field('description');
                        ?>
                        <div class="item_product col-xl-4 col-lg-4 col-md-12 col-xs-12">
                            <img src="<?php echo $product_img ?>" alt="<?php echo $product_title ?>">
                            <div class="info_item">
                                <h3 class="name_item"><a
                                            href="<?php echo $product_url ?>"><?php echo $product_title ?></a>
                                </h3>
                                <div class="mota_item">
                                    <?php echo nl2br(strip_tags($product_description)) ?>
                                </div>
                                <div class="chitiet_item"><a href="<?php echo $product_url ?>">Chi Tiết <i
                                                class="icofont-swoosh-right"></i></a></div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="pagination text-center">
                    <?php post_pagination(2); ?>
                </div>
                <?php
                // Reset postdata
                wp_reset_postdata();
                // Reset main query object
                $wp_query = NULL;
                $wp_query = $temp_query;
                ?>
            <?php else: ?>
                <h3 class="text-center">Hiện tại chưa có sản phẩm nào</h3>
            <?php endif; ?>

        </div>
    </div>
</div>
<!-- /field home -->
<?php get_footer() ?>
