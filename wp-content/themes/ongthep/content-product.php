<?php
$get_template_directory = get_template_directory_uri();
?>
<?php get_template_part('template-parts/content', 'banner'); ?>
<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$product_args = array(
    'post_type' => 'san-pham',
    'orderby' => 'updated_at',
    'order' => 'DESC',
    'post_status' => 'publish',
    'paged' => $paged,
    'posts_per_page' => 9,
);
$product_query = new WP_Query($product_args);
// Pagination fix
$temp_query = $wp_query;
$wp_query   = NULL;
$wp_query   = $product_query;
?>
<?php if ($product_query->have_posts()): ?>
    <!-- field home -->
    <div class="product">
        <div class="post-detail content">
            <div class="container">
                <div class="row display-flex">
                    <?php while ($product_query->have_posts()): ?>
                        <?php
                        $product_query->the_post();
                        $product_url = get_permalink($post->ID);
                        $product_img = get_the_post_thumbnail_url($post, 'product_thumb');
                        $product_title = $post->post_title;
                        $product_description = get_field('description');
                        ?>
                        <div class="item_product col-xl-4 col-lg-4 col-md-12 col-xs-12">
                            <img src="<?php echo $product_img ?>" alt="<?php echo $product_title ?>">
                            <div class="info_item">
                                <h3 class="name_item"><a
                                            href="<?php echo $product_url ?>"><?php echo $product_title ?></a>
                                </h3>
                                <div class="mota_item">
                                    <?php echo nl2br(strip_tags($product_description)) ?>
                                </div>
                                <div class="chitiet_item"><a href="<?php echo $product_url ?>">Chi Tiết <i
                                                class="icofont-swoosh-right"></i></a></div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="pagination text-center">
                    <?php post_pagination(2); ?>
                </div>

                <?php
                // Reset postdata
                wp_reset_postdata();
                // Reset main query object
                $wp_query = NULL;
                $wp_query = $temp_query;
                ?>
            </div>
        </div>
    </div>
    <!-- /field home -->
<?php endif; ?>