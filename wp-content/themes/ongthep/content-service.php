<?php
$get_template_directory = get_template_directory_uri();
?>
<?php get_template_part('template-parts/content', 'banner'); ?>
<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$service_args = array(
    'post_type' => 'dich-vu',
    'orderby' => 'updated_at',
    'order' => 'DESC',
    'post_status' => 'publish',
    'paged' => $paged,
    'posts_per_page' => 8,
);
$service_query = new WP_Query($service_args);
// Pagination fix
$temp_query = $wp_query;
$wp_query   = NULL;
$wp_query   = $service_query;
?>
<?php if ($service_query->have_posts()): ?>
    <!-- field home -->
    <div class="product">
        <div class="post-detail content">
            <div class="container">
                <div class="row display-flex">
                    <?php while ($service_query->have_posts()): ?>
                        <?php
                        $service_query->the_post();
                        $service_url = get_permalink($post->ID);
                        $service_img = get_the_post_thumbnail_url($post, 'product_thumb');
                        $service_title = $post->post_title;
                        $service_description = get_field('description');
                        ?>
                        <div class="item_product col-xl-3 col-lg-3 col-md-6 col-xs-12">
                            <img src="<?php echo $service_img ?>" alt="<?php echo $service_title ?>">
                            <div class="info_item">
                                <h3 class="name_item"><a
                                            href="<?php echo $service_url ?>"><?php echo $service_title ?></a>
                                </h3>
                                <div class="mota_item">
                                    <?php echo nl2br(strip_tags($service_description)) ?>
                                </div>
                                <div class="chitiet_item"><a href="<?php echo $service_url ?>">Chi Tiết <i
                                                class="icofont-swoosh-right"></i></a></div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="pagination text-center">
                    <?php post_pagination(2); ?>
                </div>

                <?php
                // Reset postdata
                wp_reset_postdata();
                // Reset main query object
                $wp_query = NULL;
                $wp_query = $temp_query;
                ?>
            </div>
        </div>
    </div>
    <!-- /field home -->
<?php endif; ?>
<?php wp_reset_query(); ?>