<?php get_header() ?>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5e48b52a023b96bc"></script>
<?php
$get_template_directory = get_template_directory_uri();
?>
<?php get_template_part('template-parts/content', 'banner'); ?>
<?php
the_post();
$directory = get_template_directory_uri();
$news_name = get_the_title();
$news_content = get_the_content();
?>

<div class="product">
    <div class="post-detail content">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <?php echo nl2br($news_content); ?>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                    <div class="addthis_inline_share_toolbox" style="padding-top: 15px !important;"></div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <?php get_sidebar('category'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php wp_reset_query(); ?>
<?php get_footer() ?>
