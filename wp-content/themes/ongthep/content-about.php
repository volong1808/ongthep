<?php get_header() ?>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5de7244eccaf8a18"></script>
<?php
$get_template_directory = get_template_directory_uri();
?>
<?php get_template_part('template-parts/content', 'banner'); ?>
<?php
the_post();
$directory = get_template_directory_uri();
$post_name = get_the_title();
$post_content = get_the_content();
$roomID = $post->ID;
$post_query = new WP_Query($product_args);
$post_image = get_the_post_thumbnail_url($post, 'product_slide');
$post_image_thumb = get_the_post_thumbnail_url($post, 'product_slide_thumb');
?>
    <div class="post-detail content">
        <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="post__content">
                    <?php echo $post_content; ?>
                </div>
<!--                <div class="social__share">-->
<!--                     Go to www.addthis.com/dashboard to customize your tools -->
<!--                    <div class="addthis_inline_share_toolbox"></div>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>
