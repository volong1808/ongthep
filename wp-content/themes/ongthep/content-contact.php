<?php
$get_template_directory = get_template_directory_uri();
?>
<?php get_template_part('template-parts/content', 'banner'); ?>
<div class="contact-page content">
    <div class="contact-page__content container">
        <div class="row">
            <div class="col-md-8">
                <h3 class="contact-page__title">Liên Hệ Với Chúng Tôi</h3>
                <div class="contact-page__description">
                    <p>Câu hỏi? Bình luận? Chúng tôi đánh giá cao phản hồi của bạn về các sản phẩm, dịch vụ và trang web
                        của chúng tôi.</p>
                    <p>Đối với yêu cầu bán hàng hoặc hỗ trợ kỹ thuật, xin vui lòng điền vào form bên dưới</p>
                </div>
                <?php echo do_shortcode( '[contact-form-7 id="170" title="Contact form 1"]' ); ?>
            </div>
            <div class="col-md-4">
                <h3 class="contact-page__title">Thông Tin Công Ty</h3>
                <div class="contact-page-info">
                    <div class="info-item">
                        <div class="info-item__content">
                            <p class="info-item__text"><i class="icofont-location-pin"></i><span>Địa chỉ: <?php echo get_option('address') ?></span></p>
<!--                            <p class="info-item__text"><i class="icofont-phone"></i><span>Điện thoại: <a href="tel:--><?php //echo get_option('phone') ?><!--">--><?php //echo get_option('phone') ?><!--</a></span></p>-->
                            <p class="info-item__text"><i class="icofont-smart-phone"></i><span>Hotline: <a href="tel:<?php echo get_option('hot_line1') ?>"><?php echo get_option('hot_line1') ?></a></span>
                                <?php
                                if (!empty(get_option('user_contact'))) {
                                    echo "<span>&nbsp;-&nbsp;". get_option('user_contact') ."</span>";
                                }
                                ?>
                            </p>
                            <p class="info-item__text"><i class="icofont-send-mail"></i><span>E-mail : <a href="mailto:<?php echo get_option('email') ?>"><?php echo get_option('email') ?></a></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--        <div class="row">-->
<!--            <div class="col-md-12 contact-page__map">-->
<!--                <iframe src="--><?php //echo get_option('maps') ?><!--" width="100%" height="300" frameborder="0"-->
<!--                        style="border:0;" allowfullscreen=""></iframe>-->
<!--            </div>-->
<!--        </div>-->
    </div>
</div>
