<?php
$get_template_directory = get_template_directory_uri();
?>
<?php get_template_part('template-parts/content', 'banner'); ?>
<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$news_args = array(
    'post_type' => 'tin-tuc',
    'orderby' => 'updated_at',
    'order' => 'DESC',
    'post_status' => 'publish',
    'paged' => $paged,
    'posts_per_page' => 9,
);
$news_query = new WP_Query($news_args);
// Pagination fix
$temp_query = $wp_query;
$wp_query   = NULL;
$wp_query   = $news_query;
?>
<?php if ($news_query->have_posts()): ?>
    <!-- field home -->
    <div class="product">
        <div class="post-detail content">
            <div class="container">
                <div class="row">
                    <?php while ($news_query->have_posts()): ?>
                        <?php
                        $news_query->the_post();
                        $news_url = get_permalink($post->ID);
                        $news_img = get_the_post_thumbnail_url($post, 'product_thumb');
                        $news_title = $post->post_title;
                        $news_date = $post->post_date;
                        ?>
                        <div class="man_news_grid_item col-xl-4 col-lg-4 col-md-12 col-xs-12">
                            <a href="<?php echo $news_url; ?>" class="man_news_item_link"></a>
                            <div class="man_news_item_img">
                                <img src="<?php echo $news_img; ?>" alt="">
                                <div class="man_news_item_over"></div>
                                <div class="man_news_item_cont">
                                    <div class="man_news_item_title">
                                        <h3><?php echo $news_title; ?></h3>
                                    </div>
                                    <div class="man_news_item_date"><i class="icofont-clock-time"></i>
                                        <span><?php echo $news_date; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="pagination text-center">
                    <?php post_pagination(2); ?>
                </div>

                <?php
                // Reset postdata
                wp_reset_postdata();
                // Reset main query object
                $wp_query = NULL;
                $wp_query = $temp_query;
                ?>
            </div>
        </div>
    </div>
    <!-- /field home -->
<?php endif; ?>
<?php wp_reset_query(); ?>