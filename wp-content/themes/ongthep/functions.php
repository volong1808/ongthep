<?php
/**
 * ongthep functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ongthep
 */

if ( ! function_exists( 'ongthep_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ongthep_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ongthep, use a find and replace
		 * to change 'ongthep' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'ongthep', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'ongthep' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'ongthep_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

        add_image_size('slide_image', 1980, 1100);
        add_image_size('slide_image_thumb', 1400, 780);
        add_image_size('about_top', 1700, 450);
        add_image_size('product_slide', 600, 600);
        add_image_size('product_slide_thumb', 150, 150);
        add_image_size('field_word_thumb', 400, 400);
        add_image_size('product_thumb', 350, 350);
        add_image_size('news_highlight_thumb', 100, 70);
        add_image_size('partner_thumb', 77, 77);
        add_image_size('home_galleries_thumb', 140, 95);
        add_image_size('icon', 100, 100);
        add_image_size('partner_image', 211, 122);
	}
endif;
add_action( 'after_setup_theme', 'ongthep_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ongthep_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'ongthep_content_width', 640 );
}
add_action( 'after_setup_theme', 'ongthep_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ongthep_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ongthep' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ongthep' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'ongthep_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ongthep_scripts() {
	wp_enqueue_style( 'ongthep-style', get_stylesheet_uri() );

	wp_enqueue_script( 'ongthep-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'ongthep-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ongthep_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Create the Slide post type
 */
function create_slide_post_type() {
    $labels = array(
        'name' => 'Slide',
        'singular_name' => 'Slide'
    );
    $args = array(
        'labels' => $labels,
        'supports' => array(
            'title',
            'thumbnail',
        ),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'menu_position' => 6,
        'can_export' => true,
        'menu_icon' => "dashicons-image-filter",
        'publicly_queryable' => true,
        'capability_type' => 'post'
    );
    register_post_type('slide', $args);
}
add_action('init', 'create_slide_post_type');


/**
 * Create the Service post type
 */
function create_service_post_type() {
    $labels = array(
        'name' => 'Dịch Vụ',
        'singular_name' => 'service'
    );
    $args = array(
        'labels' => $labels,
        'supports' => array(
            'title',
            'thumbnail',
            'editor',
        ),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'menu_position' => 4,
        'can_export' => true,
        'menu_icon' => "dashicons-cart",
        'publicly_queryable' => true,
        'capability_type' => 'post'
    );
    register_post_type('dich-vu', $args);
}
add_action('init', 'create_service_post_type');

/**
* Create the Service post type
*/
function create_news_post_type() {
    $labels = array(
        'name' => 'Tin Tức',
        'singular_name' => 'news'
    );
    $args = array(
        'labels' => $labels,
        'supports' => array(
            'title',
            'thumbnail',
            'editor',
        ),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'menu_position' => 5,
        'can_export' => true,
        'menu_icon' => "dashicons-admin-post",
        'publicly_queryable' => true,
        'capability_type' => 'post'
    );
    register_post_type('tin-tuc', $args);
}
add_action('init', 'create_news_post_type');

/**
 * Create the Product post type
 */
function create_product_post_type() {
    $labels = array(
        'name' => 'Sản Phẩm',
        'singular_name' => 'product'
    );
    $args = array(
        'labels' => $labels,
        'supports' => array(
            'title',
            'thumbnail',
            'editor',
        ),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'menu_position' => 3,
        'can_export' => true,
        'menu_icon' => "dashicons-products",
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'taxonomies' => array( 'category' )
    );
    register_post_type('san-pham', $args);
}
add_action('init', 'create_product_post_type');

/**
 * Create the partner post type
 */
function create_partner_post_type() {
    $labels = array(
        'name' => 'Đối Tác',
        'singular_name' => 'partner'
    );
    $args = array(
        'labels' => $labels,
        'supports' => array(
            'title',
            'thumbnail',
        ),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'menu_position' => 8,
        'can_export' => true,
        'menu_icon' => "dashicons-performance",
        'publicly_queryable' => true,
        'capability_type' => 'post'
    );
    register_post_type('partner', $args);
}
add_action('init', 'create_partner_post_type');

/**
 * Create the About post type
 */
function create_about_post_type() {
    $labels = array(
        'name' => 'Giới Thiệu',
        'singular_name' => 'about'
    );
    $args = array(
        'labels' => $labels,
        'supports' => array(
            'title',
            'thumbnail',
            'editor',
        ),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'menu_position' => 7,
        'can_export' => true,
        'menu_icon' => "dashicons-book",
        'publicly_queryable' => true,
        'capability_type' => 'post'
    );
    register_post_type('gioi-thieu', $args);
}
add_action('init', 'create_about_post_type');

add_action('admin_menu', 'ongthep_information');

/**
 * Function add menu Thông Tin Công Ty
 */
function ongthep_information(){
    add_menu_page(
        'Thông Tin Công Ty',
        'Thông Tin Công Ty',
        'manage_options',
        'info-company',
        'ongthep_information_page',
        '',
        2
    );
}

/**
 * Function show Thong tin cong ty page
 */
function ongthep_information_page() {
    if (!current_user_can('manage_options'))
    {
        wp_die( __('You do not have sufficient permissions to access this page.') );
    }
    require_once get_template_directory() . '/inc/form-ongthep-information.php';
}

// following are code adapted from Custom Post Type Category Pagination Fix by jdantzer
function fix_category_pagination($qs)
{
    if (isset($qs['category_name']) && isset($qs['paged'])) {
        $qs['post_type'] = get_post_types($args = array(
            'public' => true,
            '_builtin' => false
        ));
        array_push($qs['post_type'], 'post');
    }
    return $qs;
}
add_filter('request', 'fix_category_pagination');

function remove_page_from_query_string($query_string)
{
    if ($query_string['name'] == 'page' && isset($query_string['page'])) {
        unset($query_string['name']);
        $query_string['paged'] = $query_string['page'];
    }
    return $query_string;
}
add_filter('request', 'remove_page_from_query_string');

function custom_posts_per_page( $query ) {
    //fixing pagination of custom post type
    if ( $query->is_archive('san-pham')) {
        set_query_var('posts_per_page', 1);
    }
}
add_action( 'pre_get_posts', 'custom_posts_per_page' );

if ( ! function_exists( 'post_pagination' ) ) :
    function post_pagination( $mid_size ) {
        global $wp_query;
        $pager = 999999999; // need an unlikely integer

        echo paginate_links( array(
            'base' => str_replace( $pager, '%#%', esc_url( get_pagenum_link( $pager ) ) ),
            'format' => '?paged=%#%',
            'mid_size' => $mid_size,
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages,
            'prev_text' => __('&laquo;'),
            'next_text' => __('&raquo;'),
        ) );
    }
endif;

// prioritize pagination over displaying custom post type content
add_action('init', function() {
    add_rewrite_rule(
        '(.?.+?)/page/?([0-9]{1,})/?$',
        'index.php?pagename=$matches[1]&paged=$matches[2]',
        'top'
    );
});

function searchfilter($query) {
    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('san-pham'));
        $query->set('paged', ( get_query_var('paged') ) ? get_query_var('paged') : 1 );
        $query->set('posts_per_page',9);
    }
    return $query;
}
add_filter('pre_get_posts','searchfilter');
