<?php
$partner_args = array(
    'post_type' => 'partner',
    'orderby' => 'updated_at',
    'order' => 'DESC'
);
$partner_query = new WP_Query($partner_args);
?>
<?php if ($partner_query->have_posts()): ?>
<div class="box_doitac">
    <?php while ($partner_query->have_posts()): ?>
        <?php
        $partner_query->the_post();
        $partner_img = get_the_post_thumbnail_url($post, 'partner_image');
        ?>
        <div class="item_doitac col-xl-4 col-lg-4  col-md-6 col-xs-6">
            <div class="gallery_dt">
                <img src="<?php echo $partner_img ?>" alt="<?php echo $post->post_title; ?>">
            </div>
        </div>
    <?php endwhile; ?>
    <div class="clear"></div>
</div>
<?php else: ?>
    <div class="box_doitac">
        <span>Hiện tại chưa có đối tác nào</span>
    </div>
<?php endif;?>
<?php wp_reset_query(); ?>