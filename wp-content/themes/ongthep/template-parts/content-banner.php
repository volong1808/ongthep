<header class="content-page__header">
</header>
    <?php
        $get_template_directory = get_template_directory_uri();
        $bannerImage = $get_template_directory . '/images/banner.png';
        $banner = get_field('banner');
        if (!empty($banner)) {
            $bannerImage = $banner['sizes']['slide_image'];
        }
    ?>
    <div class="banner-page"
         style="background-image: url('<?php echo $bannerImage ?>')">
        <div class="banner-page__content">
            <div class="banner-page__body">
                <?php the_title('<h2 class="banner-page__title">', '</h2>'); ?>
            </div>
        </div>
    </div>
</header>